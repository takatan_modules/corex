const { Takatan, Log, Kefir } = require('@takatanjs/core')

const log = Log.m('WorkerManagerModule')

const AbstractWorkerManager = require('./wm.abstract')

class WorkerManagerModule extends Takatan.extends('AbstractFactoryCoreModule')
{
  constructor(parent)
  {
    super(parent)
    this.factoryInit('wms',AbstractWorkerManager)
    this._wms = {}
  }

  registerWM(Wm)
  {
    this.factoryRegisterClass('wms',Wm)
    let inf = Wm.info()
    this._wms[inf.code] = new Wm(this)
  }

  moduleInit()
  {
    return Kefir.stream(obs=>{
      let $t = Object.keys(this._wms).map(wmc=>this._wms[wmc].wmInit())
      this.obsEnd(obs,$t)
    })
    .flatMap($t=>Kefir.concat($t))
    .last()
  }

  moduleStart()
  {
    return Kefir.stream(obs=>{
      let $t = Object.keys(this._wms).map(wmc=>this._wms[wmc].wmStart())
      this.obsEnd(obs,$t)
    })
    .flatMap($t=>Kefir.concat($t))
    .last()
  }

  wm(code)
  {
    return this._wms[code] || null
  }
}

module.exports = Takatan.register(WorkerManagerModule)
