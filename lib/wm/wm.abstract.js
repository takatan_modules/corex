const { Takatan, Log, Kefir } = require('@takatanjs/core')

const log = Log.m('AbstractWorkerManagerLayer')

class AbstractWorkerManager extends Takatan.extends('AbstractCoreObject')
{

  static info()
  {
    return {
      code: 'abstractwm',
      name: 'Abstract Workers Manager'
    }
  }

  wmInit()
  {
    return Kefir.constant(true)
  }

  wmStart()
  {
    return Kefir.constant(true)
  }

  wmWorkersList(opts={})
  {
    return Kefir.constantError({code:'wml.workersList should be reimplemented by other modules'})
  }

  wmCreateWorker(opts={})
  {
    return Kefir.constantError({code:'wml.createWorker should be reimplemented by other modules'})
  }

  wmDeleteWorker(wcode,opts={})
  {
    return Kefir.constantError({code:'wml.deleteWorker should be reimplemented by other modules'})
  }

  wmStartWorker(wcode,opts={})
  {
    return Kefir.constantError({code:'wml.startWorker should be reimplemented by other modules'})
  }

  wmStopWorker(wcode,opts={})
  {
    return Kefir.constantError({code:'wml.stopWorker should be reimplemented by other modules'})
  }
}

module.exports = Takatan.register(AbstractWorkerManager)
