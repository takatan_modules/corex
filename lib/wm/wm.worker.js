const { Takatan, Log, Kefir } = require('@takatanjs/core')

const log = Log.m('AbstractWorker')

class AbstractWorker extends Takatan.extends('AbstractCoreObject')
{

  /**
  wmlWorkersList(opts={})
  {
    return Kefir.constantError({code:'wml.workersList should be reimplemented by other modules'})
  }

  wmlCreateWorker(opts={})
  {
    return Kefir.constantError({code:'wml.createWorker should be reimplemented by other modules'})
  }

  wmlDeleteWorker(wcode,opts={})
  {
    return Kefir.constantError({code:'wml.deleteWorker should be reimplemented by other modules'})
  }

  wmlStartWorker(wcode,opts={})
  {
    return Kefir.constantError({code:'wml.startWorker should be reimplemented by other modules'})
  }

  wmlStopWorker(wcode,opts={})
  {
    return Kefir.constantError({code:'wml.stopWorker should be reimplemented by other modules'})
  }
  /**/
}

module.exports = Takatan.register(AbstractWorker)
