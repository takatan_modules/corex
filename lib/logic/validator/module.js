const { Takatan, Log, Kefir } = require('@takatanjs/core')

const log = Log.m('LogicValidatorModule')

class LogicValidatorModule extends Takatan.extends('AbstractFactoryCoreModule')
{
  constructor(parent)
  {
    super(parent)
    //this.factoryInit(
  }

}

module.exports = Takatan.register(LogicValidatorModule)
