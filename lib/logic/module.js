const { Takatan, Log, Kefir } = require('@takatanjs/core')

const log = Log.m('LogicModule')

class LogicModule extends Takatan.extends('AbstractFactoryCoreModule')
{
  constructor(parent)
  {
    super(parent)
    //this.factoryInit(
  }

}

module.exports = Takatan.register(LogicModule)
