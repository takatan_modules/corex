/*
  This file is part of TakatanJS software.
  @author abrakadobr
  @version 2.5.57
  @website https://js.takatan.tk
*/


let Rx = require('rxjs')
let RxO = require('rxjs/operators')

let log = require('../../log').m('Context')
let g18n = require('../../g18n')

const TestableObject = require('../logic/abstract/testable')
const VirtualNode = require('../node/vnode')

const FieldSelector = require('../logic/abstract/context_field')
const ListSelector = require('../logic/abstract/context_list')

class Context extends VirtualNode
{

  constructor(parent,owner,empty=false)
  {
    super(parent)
    this._createdEmpty = empty

    this._contextNodes = []
    this._session = null
    this._type = 'context'
    this.done = new Rx.Subject()

    let proj = new VirtualNode(this)
    //this.a
    proj.addField({code:'name',type:'string'})
    proj.addField({code:'coreversion',type:'string'})
    proj.f('name',this.M('config').conf('project.name'))
    proj.f('coreversion',this.M('core').version())

    this.addNode(proj,'project')

    this.addField({name:g18n.t('Owner'),code:'owner',type:'string',flags:['readonly','virtual','logic','template']})
    this.f('owner',owner)

    this.addField({name:g18n.t('Context users'),code:'users',type:'relation',conf:{ops:[{source:'manual',manual:['user']},{source:'manual',manual:'username, firstName, lastName'}]},flags:['readonly','virtual','logic','template'],quantity:0})

    if (empty)
      return
    this.addField({name:g18n.t('NotFoundField'),code:'f404',type:'any',flags:['readonly','virtual','logic','template']})

    this.addField({name:g18n.t('Depth'),code:'depth',type:'number',flags:['readonly','virtual','logic','template']})

    this.addField({name:g18n.t('Sended'),code:'sended',type:'number',flags:['readonly','virtual','logic','template']})
    this.addField({name:g18n.t('Pushed'),code:'pushed',type:'number',flags:['readonly','virtual','logic','template']})
    this.addField({name:g18n.t('Executed'),code:'executed',type:'number',flags:['readonly','virtual','logic','template']})
    this.addField({name:g18n.t('Error executed'),code:'error',type:'number',flags:['readonly','virtual','logic','template']})

    this.addField({name:g18n.t('Success test'),code:'success',type:'number',flags:['readonly','virtual','logic','template']})
    this.addField({name:g18n.t('Fail test'),code:'fail',type:'number',flags:['readonly','virtual','logic','template']})


    this.addField({name:g18n.t('Bot'),code:'bot',type:'relation',conf:{ops:[{source:'manual',manual:['user']},{source:'manual',manual:'username, firstName, lastName'}]},flags:['readonly','virtual','logic','template'],quantity:0})

    this.addField({name:g18n.t('Context chats'),code:'chats',type:'relation',conf:{ops:[{source:'manual',manual:['tgchat']},{source:'manual',manual:'username, title'}]},flags:['readonly','virtual','logic','template'],quantity:0})
    this.addField({name:g18n.t('Message'),code:'message',type:'relation',conf:{ops:[{source:'manual',manual:['tgmessage']},{source:'manual',manual:'text'}]},flags:['readonly','virtual','logic','template'],quantity:1})
    this.addField({name:g18n.t('Screen'),code:'screen',type:'relation',conf:{ops:[{source:'manual',manual:['screen']},{source:'manual',manual:'title'}]},flags:['readonly','virtual','logic','template'],quantity:1})
    this.addField({name:g18n.t('Raw'),code:'raw',type:'any',flags:['readonly','virtual','logic','template']})
    this.addField({name:g18n.t('Update'),code:'upd',type:'any',flags:['readonly','virtual','logic','template']})
    this.addField({name:g18n.t('Command'),code:'cmd',type:'any',flags:['readonly','virtual','logic','template']})
    this.addField({name:g18n.t('Cron'),code:'cron',type:'any',flags:['readonly','virtual','logic','template']})

    this.f('sended',0)
    this.f('pushed',0)
    this.f('executed',0)
    this.f('success',0)
    this.f('error',0)
    this.f('fail',0)
    this.f('bot',this.M('telegram').me())
    this.f('cron',this.M('cron').logicFields())
  }

  cloneDepth()
  {
    let ret = new Context(this._parent,this._owner)

    ret.f('depth',this.f('depth')+1)
    ret._session = this._session
    //from virtual node
    ret._contextNodes = this._contextNodes
    ret._fields = this._fields
    ret._model = this._model
    ret._populated = this._populated
    return ret
  }

  cloneFull()
  {
    let ret = new Context(this._parent,this._owner,this._createdEmpty)
    ret._session = this._session
    ret._contextNodes = this._contextNodes
    ret._fields = this._fields
    ret._model = this._model
    ret._populated = this._populated
    return ret
  }

  addNode(node,as=null)
  {
    if (!this.vnode(as))
      this._contextNodes.push({
        node: node,
        as: as
      })
    else
      for(let i=0;i<this._contextNodes.length;i++)
        if (this._contextNodes[i].as == as)
          this._contextNodes[i].node = node
  }

  vnode(code)
  {
    let ret = null
    this._contextNodes.forEach(x=>{
      //console.log([code,x.as])
      if (code==x.as)
        ret = x.node
    })
    return ret
    //return this._contextNodes.reduce((acc,x)=>x.as==code?x.node:null,null)
  }
  //======== virtuals
  //session
  session(sess)
  {
    if(typeof sess != 'undefined')
      this._session = sess
    return this._session
  }

  sfield(code,val)
  {
    let sess = this.session()
    //console.log(sess,code,val)
    if (!sess) return
    if (typeof val != 'undefined')
      sess[code] = val
    return sess[code]
  }

  sdrop(code)
  {
    let sess = this.session()
    if (!sess) return
    delete sess[code]
  }
  /**/

  //logic
  ctxLogicFields()
  {
    let ret = []
    let pass = ['cron','cmd','screen','message','users','chats','bot','raw','upd']
    for(let fcode in this._fields)
    {
      if (pass.includes(fcode))
        continue
      let f = this.field(fcode)
      //console.log([[f],this._model])
      let scope = 'virtual'
      let ctxf = ['owner','depth','sended','pushed','executed','error','success','fail']
      if (ctxf.includes(f._info.code))
        scope = 'context'
      ret.push({
        scope: scope,
        info: f._info,
        values: f.all()
      })
    }

    //this._cron.forEach(x=>ret.push(x))
 
    return ret
  }

  logicFields()
  {
    let fields = this.ctxLogicFields()
    if (this.f('cmd'))
      this.f('cmd').logicFields().forEach(f=>{
        f.scope = 'cmd'
        fields.push(f)
      })
    if (this.f('cron'))
      this.f('cron').forEach(f=>fields.push(f))
    if (this.f('screen'))
      this.f('screen').logicFields().forEach(f=>{
        f.scope = 'screen'
        fields.push(f)
      })
    if (this.f('message'))
      this.f('message').logicFields().forEach(f=>{
        f.scope = 'message'
        fields.push(f)
      })
    if (this.f('users'))
      this.field('users').forEach((u,i)=>{
        u.logicFields().forEach(f=>{
          if (i) f.scope+=''+i
          fields.push(f)
        })
      })
    if (this.f('chats'))
      this.field('chats').forEach((c,i)=>{
        if (c && typeof c.logicFields == 'function')
          c.logicFields().forEach(f=>{
            if (i) f.scope+=''+i
            fields.push(f)
          })
      })
    let botProfile = this.M('telegram').me()
    if (this.f('bot'))
      this.f('bot').logicFields().forEach(f=>{
        f.scope = 'bot'
        fields.push(f)
      })
    this._contextNodes.forEach(inc=>{
      inc.node.logicFields().forEach(f=>{
        if (inc.as)
          f.scope = inc.as
        fields.push(f)
      })
    })
    return fields
  }

  logicFieldByPath(path)
  {
    let fields = this.logicFields()
    let paths = path.split('.')
    if (paths.length!=2)
    {
      log.err('logicFieldByPath error!',path)
      return null
    }
    if(paths[0]=='generator')
    {
      let Gen = this.M('field').getGeneratorClass(paths[1])
      if (!Gen) return null
      let genInfo = Gen.info()
      let gen = new Gen(this)
      let vv = gen.generateSync()
      let ret = {
        scope: 'generator',
        info: {
          flags: ['virtual','readonly'],
          type: genInfo.types[0],
          code: paths[1],
          quantity: 1,
          name: genInfo.name,
          conf: {ops:[]}
        },
        values: [vv],
        subs: []
      }
      return ret
    }
    return fields.reduce((acc,f)=>{
      return f.scope==paths[0]&&f.info.code==paths[1]?f:acc
    },null)
  }

  //_ftv(f,i=0)
  //{
    //if (typeof f.values[i] == 'undefined') return null
    //return f.values[i]
  //}

  user(i=0)
  {
    let f = this.field('users')
    if (!f) return null
    return this.field('users').getp(i)
  }

  users(val)
  {
    let f = this.field('users')
    if (!f) return null
    return this.field('users').all(val)
  }

  chat(i=0)
  {
    let f = this.field('chats')
    if (!f) return null
    return this.field('chats').getp(i)
  }

  chats(val)
  {
    let f = this.field('chats')
    if (!f) return null
    return this.field('chats').all(val)
  }

  type(val)
  {
    if (val) this._type = val
    return this._type
  }

  cmd(val)
  {
    let f = this.field('cmd')
    if (!f) return null
    return this.f('cmd',val)
  }

  message(val)
  {
    let f = this.field('message')
    if (!f) return null
    return this.f('message',val)
  }

  screen(val)
  {
    let f = this.field('screen')
    if (!f) return null
    return this.f('screen',val)
  }

  raw(val)
  {
    let f = this.field('raw')
    if (!f) return null
    return this.f('raw',val)
  }

  upd(val)
  {
    let f = this.field('upd')
    if (!f) return null
    return this.f('upd',val)
  }

  /////////////////////
  ////    SELECTORS
  /////////////////////
  fieldSelector(cfg,info)
  {
    return new FieldSelector(this,cfg,info)
  }

  listSelector(info)
  {
    return new ListSelector(this,info)
  }

  fieldByPath(path)
  {
    return Rx.Observable.create(obs=>{
      let obj = null
      let field = null
      if (!path.includes('.'))
        return this.obsEnd(obs,this)
      let paths = path.split('.')
      let scope = paths[0]
      let fcode = paths[1]
      if (scope=='generator')
      {
        let Gen = this.M('field').getGeneratorClass(fcode)
        return this.obsEnd(obs,null)
      }
      if (scope=='user')
        obj = this.user()
      if (scope=='tgchat')
        obj = this.chat()
      if (scope=='cmd')
        obj = this.cmd()
      if (scope=='screen')
        obj = this.screen()
      if (scope=='message')
        obj = this.message()
      if (!obj)
        obj = this._contextNodes.reduce((acc,inc)=>inc.as==scope?inc.node:acc,null)
      //console.log(['ctxField',scope,fcode,!!obj])
      if (!obj)
        return this.obsEnd(obs,null)
      if (typeof obj.field != 'function')
        return this.obsEnd(obs,null)
      let f = obj.field(fcode)
      return this.obsEnd(obs,f)
    })
  }

  nodeByPath(path,fieldNoCare=false)
  {
    return Rx.Observable.create(obs=>{
      let obj = null
      let field = null
      if (!path.includes('.')&&!fieldNoCare)
        return this.obsEnd(obs,this)
      let paths = path.split('.')
      let scope = paths[0]
      let fcode = paths[1]
      if (scope=='generator')
      {
        let Gen = this.M('field').getGeneratorClass(fcode)
        return this.obsEnd(obs,null)
      }
      if (scope=='user')
        obj = this.user()
      if (scope=='tgchat')
        obj = this.chat()
      if (scope=='cmd')
        obj = this.cmd()
      if (scope=='screen')
        obj = this.screen()
      if (scope=='message')
        obj = this.message()
      if (scope=='context')
        obj = this
      if (scope=='virtual')
        obj = this
      if (!obj)
        obj = this._contextNodes.reduce((acc,inc)=>inc.as==scope?inc.node:acc,null)
      //if (path == 'replyUser.id')
      if (!obj)
      {
        obj = this
        fcode = 'f404'
      }
      if (!obj)
        return this.obsEnd(obs,null)
      {
        //console.log(this._contextNodes)
        //console.log(['contextNode',path])
      }
        //return this.obsEnd(obs,null)
      if (fieldNoCare)
        return this.obsEnd(obs,obj)
      if (typeof obj.field != 'function')
        return this.obsEnd(obs,null)
      let f = obj.field(fcode)
      if (f)
        return this.obsEnd(obs,{node:obj,fcode:fcode})
      return this.obsEnd(obs,null)
    })
  }

  dataByPath(path)
  {
    return Rx.Observable.create(obs=>{
      let obj = this.view()
      let paths = path.split('.')
      let cur = obj
      let ok = true
      paths.forEach(p=>{
        if (typeof cur[p] == 'undefined')
          ok = false
        if (!ok) return
        cur = cur[p]
      })
      let ret
      if (ok) 
        ret = cur
      this.obsEnd(obs,ret)
    })
  }

  selectorResult(cfg,inf,def)
  {
    let sel = this.fieldSelector(cfg,inf)
    return sel.init(this).pipe(
      RxO.catchError(err=>Rx.of(false)),
      RxO.map(x=>{
        if (!x)
          return def
        if (sel.quantity()==1)
          return sel.oneModified()
        return sel.allModified()
      })
    )
  }
}

module.exports = Context
