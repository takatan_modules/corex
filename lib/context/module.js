const { Takatan, Log, Kefir } = require('@takatanjs/core')

const log = Log.m('ContextModule')

class ContextModule extends Takatan.extends('AbstractFactoryCoreModule')
{
  constructor(parent)
  {
    super(parent)
  }

}

module.exports = Takatan.register(ContextModule)
